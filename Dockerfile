FROM golang:1.15 as builder

COPY ./ /build

WORKDIR /build/cmd/pub

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o pub .

WORKDIR /build/cmd/sub

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o sub .

FROM epbc/alpine:latest

RUN adduser -u 25010 -g 'EPBC Application Owner' -D epbc

WORKDIR /home/epbc

COPY --from=builder /build/cmd/pub/pub .
COPY --from=builder /build/cmd/sub/sub .

USER epbc

CMD ["/bin/bash","-c","while true; do sleep 60; echo > /dev/null ; done"]
#CMD ./ns-client sys_nats-1:4222 sys_nats-2:4223 sys_nats-3:4224

