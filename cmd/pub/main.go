package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/_metalogic_/config"
	"github.com/nats-io/stan.go"
)

func main() {
	servers := config.MustGetConfig("NATS_SERVERS")
	cluster := config.MustGetConfig("NATS_CLUSTER")
	sc, err := stan.Connect(
		cluster,
		"pub-client",
		stan.Pings(1, 3),
		stan.NatsURL(servers),
		stan.SetConnectionLostHandler(func(con stan.Conn, reason error) {
			log.Fatalf("Connection lost, reason: %v", reason)
		}),
	)
	if err != nil {
		log.Fatalln(err)
	}
	defer sc.Close()

	var i int64
	for {
		msg := fmt.Sprintf("%s[%d]", os.Args[1], i)
		log.Print("publishing message: " + msg)
		if err := sc.Publish("foo", []byte(msg)); err != nil {
			log.Println("failed to publish '", msg, "':", err)
			continue
		}
		// fake processing time (twice the time of the single subscriber)
		time.Sleep(20 * time.Millisecond)
		i++
	}

}
