package main

import (
	"os"
	"time"

	"bitbucket.org/_metalogic_/config"
	"bitbucket.org/_metalogic_/log"
	"github.com/nats-io/stan.go"
)

func main() {
	servers := config.MustGetConfig("NATS_SERVERS")
	cluster := config.MustGetConfig("NATS_CLUSTER")
	if len(os.Args) != 2 {
		log.Fatal("subscriber name was not provided")
	}

	subscriber := os.Args[1]
	sc, err := stan.Connect(
		cluster,
		subscriber,
		stan.Pings(1, 30),
		stan.MaxPubAcksInflight(20),
		stan.PubAckWait(5*time.Second),
		stan.NatsURL(servers),
		stan.SetConnectionLostHandler(func(con stan.Conn, reason error) {
			log.Fatalf("Connection lost, reason: %v", reason)
		}),
	)
	if err != nil {
		log.Fatal(err)
	}
	defer sc.Close()

	sub, err := sc.Subscribe("foo", func(m *stan.Msg) {
		log.Infof("subscriber %s, received message: %s\n", subscriber, string(m.Data))
		// fake processing time
		time.Sleep(time.Millisecond * 10)
		// if processing succeeds then acknowledge the message
		if err := m.Ack(); err != nil {
			log.Error(err)
		}
	}, stan.MaxInflight(10), stan.AckWait(time.Second), stan.SetManualAckMode())
	if err != nil {
		log.Fatal(err)
	}
	defer sub.Unsubscribe()

	time.Sleep(1000 * time.Hour)
}
