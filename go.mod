module bitbucket.org/_metalogic_/ns-client

go 1.15

require (
	bitbucket.org/_metalogic_/config v1.1.0
	bitbucket.org/_metalogic_/log v1.4.1
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/nats-io/nats-streaming-server v0.19.0 // indirect
	github.com/nats-io/stan.go v0.7.0
	google.golang.org/protobuf v1.25.0 // indirect
)
